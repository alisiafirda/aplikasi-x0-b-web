/*
SQLyog Community
MySQL - 10.1.28-MariaDB : Database - kantor
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kantor` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `kantor`;

/*Table structure for table `pegawai` */

CREATE TABLE `pegawai` (
  `kode` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenkel` varchar(20) DEFAULT NULL,
  `id_status` int(5) NOT NULL,
  `photos` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pegawai` */

insert  into `pegawai`(`kode`,`nama`,`jenkel`,`id_status`,`photos`) values 
('','','',0,''),
('1931733076','Alisia Firda','Perempuan',1,'poto2.jpg'),
('1931733082','Virginia Abrinsa','Perempuan',2,'poto4.jpg'),
('1931733092','Fernanda Eka Novitasari','Perempuan',2,NULL);

/*Table structure for table `status` */

CREATE TABLE `status` (
  `id_status` int(5) DEFAULT NULL,
  `status_peg` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `status` */

insert  into `status`(`id_status`,`status_peg`) values 
(1,'Honorer'),
(2,'Tetap');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
